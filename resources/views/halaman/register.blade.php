<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru !</h1>
        <h4>Sign Up Form</h4>
        <label>First name:</label><br><br>
        <input type="text" name="namadepan"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="namabelakang"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="jk" value="Male">Male <br>
        <input type="radio" name="jk" value="Female">Female <br>
        <input type="radio" name="jk" value="Female">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="wn">
            <option value="1">Indonesian</option>
            <option value="2">English</option>
            <option value="3">Other</option>
        </select> <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="lg">Bahasa Indonesia <br>
        <input type="checkbox" name="lg">English <br>
        <input type="checkbox" name="lg">Other <br><br>
        <label>Bio:</label><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="kirim">

    </form>

</body>

</html>